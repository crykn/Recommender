# Recown – a recommendation system

Recown is a simple memory-based recommender system written in Java. It features content-based and collaborative filtering. A simple hybrid approach is included as well.

* [Wiki](https://gitlab.com/eitelkalk/Recown/wikis/home)
* [Download and installation](https://gitlab.com/eitelkalk/Recown/wikis/download)
* [Getting started](https://gitlab.com/eitelkalk/Recown/wikis/simple-example)
* [Example on real dataset](https://gitlab.com/eitelkalk/Recown/wikis/pewn-example)
* [Javadoc](http://eitelkalk.gitlab.io/Recown/)