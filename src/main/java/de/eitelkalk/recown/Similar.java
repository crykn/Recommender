package de.eitelkalk.recown;

/**
 * This interface provides a measurement of how similar two objects are.
 * Similarity between objects is a key for giving good recommendations. The
 * recommendations depend on a good calculation of this similarity, accessed
 * through {@link #similarityTo(Object)}.
 * 
 * @author eitelkalk
 *
 * @param <T>
 *            What objects should this object be compared to? Most likely it is
 *            the same class as the implementing.
 */
public interface Similar<T> {

	/**
	 * Calculates the similarity between {@code this} and {@code that}, which
	 * represents how similar {@code this} and {@code that} are. The similarity
	 * must be a value between {@code 0} and {@code 1}, where {@code 0}
	 * represents that the two objects are least similar and {@code 1}
	 * represents that the objects agree on every detail.
	 * 
	 * <p>
	 * The implementor must ensure that the return value lies between {@code 0}
	 * and {@code 1}.
	 * </p>
	 * 
	 * <p>
	 * When implemented, it is recommended that this method is symmetric,
	 * i.&nbsp;e. {@code
	 * 	this.similarityTo(that) == that.similarityTo(this)
	 * }
	 * </p>
	 * 
	 * @param that
	 *            the element the similarity should be calculated to
	 * @return A value between {@code 0} and {@code 1}, ranging from not similar
	 *         to completely similar.
	 */
	double similarityTo(T that);
}
