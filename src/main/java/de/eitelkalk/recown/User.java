package de.eitelkalk.recown;

import java.util.ArrayList;
import java.util.List;

import de.eitelkalk.recown.tools.Tools;

/**
 * This class represents a user of the {@link Database}. Users can give
 * {@link Item}s {@link Rating}s and recommendations are given for single users
 * based on their previous {@link Rating}s.
 * 
 * @author eitelkalk
 */
public class User implements Similar<User> {

	private final List<Rating> ratings = new ArrayList<Rating>();
	private final List<Average<Tag>> ratedTags = new ArrayList<Average<Tag>>();
	private final double[] weight = { 1, 0, 0 };
	private double averageRating, cosineNorm, tagNorm;

	/**
	 * This {@link User} will give the {@code item} a {@link Rating} with the
	 * given {@code rating}-value. If the {@link User} has rated the
	 * {@code item} in the past, it will be overwritten. If there are multiple
	 * ways to rate an {@link Item}, this should already be represented in the
	 * given {@code rating}.
	 * 
	 * @param item
	 *            the {@link Item} the {@link User} will rate
	 * @param rating
	 *            the value the {@link User} will rate the {@link Item}
	 */
	public void rate(Item item, double rating) {
		Rating currentRating = getRatingForItem(item);
		if (currentRating == null) {
			ratings.add(new Rating(item, rating));
			updateAverageRating();
		} else {
			removeRatingFromTags(item, currentRating.getValue());
			currentRating.setValue(rating);
		}
		updateCosineNorm();
		addRatingToTags(item, rating);
		updateTagNorm();
	}

	/**
	 * Should be set for all users simultaneously.
	 * 
	 * @param weights
	 *            the weights that should be used for calculating the similarity
	 *            to another {@link User}. Negative values are ignored.
	 * 
	 * @see Database#setWeightsForUserSimilarityCalculation(double...)
	 * @see #similarityTo(User)
	 */
	protected void setWeightsForSimilarityCalculation(double... weights) {
		for (int i = 0; i < weights.length && i < this.weight.length; i++) {
			double w = weights[i];
			if (w >= 0) {
				this.weight[i] = w;
			}
		}
	}

	private void addRatingToTags(Item item, double value) {
		for (Tag tag : item) {
			Average<Tag> average = findAverageForTag(ratedTags, tag);
			if (average != null) {
				average.addValue(value);
			} else {
				ratedTags.add(new Average<Tag>(tag, value));
			}
		}
	}

	private Average<Tag> findAverageForTag(List<Average<Tag>> list, Tag tag) {
		for (Average<Tag> a : list) {
			if (a.getData().equals(tag)) {
				return a;
			}
		}
		return null;
	}

	private void removeRatingFromTags(Item item, double value) {
		for (Tag tag : item) {
			Average<Tag> average = findAverageForTag(ratedTags, tag);
			if (average != null) {
				average.removeValue(value);
			}
		}
	}

	/**
	 * If the {@link User} has rated the given {@code item}, the corresponding
	 * {@link Rating} will be returned.
	 * 
	 * @param item
	 *            the {@link Item} for which a {@link Rating} should be found
	 * @return the {@link Rating} the {@link User} has given the {@code item}
	 *         before. {@code null} if the {@link User} hasn't rated the
	 *         {@code item} yet.
	 */
	public Rating getRatingForItem(Item item) {
		for (Rating rating : ratings) {
			if (rating.getItem().equals(item)) {
				return rating;
			}
		}
		return null;
	}

	/**
	 * Returns {@code true} if the {@link User} has given a {@link Rating} to
	 * the passed {@code item} in the past.
	 * 
	 * @param item
	 *            the {@link Item} for which it should be checked whether the
	 *            {@link User} has rated it.
	 * @return {@code true} if the {@link User} already rated the {@code item}.
	 *         {@code false} if the {@link User} hasn't rated the {@code item}
	 *         yet.
	 */
	public boolean hasRated(Item item) {
		return getRatingForItem(item) != null;
	}

	/**
	 * Returns a {@link List} of the {@link Item}s the {@link User} has rated
	 * yet, sorted from best to worst rated.
	 * 
	 * @param numberOfItems
	 *            the maximum number of {@link Item}s the returned {@link List}
	 *            should contain.
	 * @return A {@link List} containing the {@link Item}s the {@link User} has
	 *         rated, sorted in descending order.
	 */
	public List<Item> getBestRatedItems(int numberOfItems) {
		List<Rating> ratings = new ArrayList<Rating>(this.ratings);

		return extractItems(Tools.getMostSimilarElements(new Rating(null, 1),
				ratings, numberOfItems));
	}

	private List<Item> extractItems(List<Rating> ratings) {
		List<Item> items = new ArrayList<Item>();
		for (Rating r : ratings) {
			items.add(r.getItem());
		}
		return items;
	}

	/**
	 * Returns a {@link List} of {@link Tag}s the {@link User} has rated. Since
	 * there is no possibility to explicitly rate {@link Tag}s (yet), the
	 * ratings are given implicitly. If the {@link User} gives an {@link Item} a
	 * {@link Rating}, the value of this {@link Rating} is applied to all the
	 * {@link Tag}s the {@link Item} contains. If a {@link Tag} is contained in
	 * several rated {@link Item}s, the average over the {@link Rating}s is
	 * taken.
	 * 
	 * @param numberOfTags
	 *            the maximum number of {@link Tag}s the returned {@link List}
	 *            should contain.
	 * @return A {@link List} containing the {@link Tag}s the {@link User} has
	 *         rated, sorted from best to worst rated.
	 */
	public List<Tag> getBestRatedTags(int numberOfTags) {
		List list = new ArrayList<Average<Tag>>();
		ratedTags.forEach(a -> list.add(a));

		return extractTags(Tools.getMostSimilarElements(
				new Average<Tag>(null, 1), list, numberOfTags));
	}

	private List<Tag> extractTags(List<Average<Tag>> list) {
		List<Tag> tags = new ArrayList<Tag>();
		list.forEach(a -> tags.add(a.getData()));
		return tags;
	}

	/**
	 * Calculates the similarity between two users. The similarity is the
	 * weighted sum of different similarity calculations. The weights can be set
	 * via {@link #setWeightsForSimilarityCalculation(double...)}.
	 * 
	 * {@inheritDoc}
	 * 
	 * @see #setWeightsForSimilarityCalculation(double...)
	 * @see Database#setWeightsForUserSimilarityCalculation(double...)
	 * @see Similar#similarityTo(Object)
	 */
	@Override
	public double similarityTo(User that) {
		double sum = 0;
		if (weight[0] != 0)
			sum += weight[0] * cosineSimilarityTo(that);
		if (weight[1] != 0)
			sum += weight[1] * pearsonCorrelationTo(that);
		if (weight[2] != 0)
			sum += weight[2] * tagSimilarityTo(that);
		double k = 0;
		for (double w : weight) {
			k += w;
		}
		double value = k == 0 ? sum : sum / k;
		return Tools.mapToInterval(value, 0, k, 0, 1);
	}

	private double cosineSimilarityTo(User that) {
		double sum = 0;
		for (Rating thisRating : this.ratings) {
			Rating thatRating = that.getRatingForItem(thisRating.getItem());
			if (thatRating != null) {
				sum += thisRating.getValue() * thatRating.getValue();
			}
		}
		return sum <= 0 ? 0 : sum / (this.cosineNorm() * that.cosineNorm());
	}

	private void updateCosineNorm() {
		double sum = 0;
		for (Rating rating : this.ratings) {
			sum += rating.getValue() * rating.getValue();
		}
		this.cosineNorm = Math.sqrt(sum);
	}

	private double cosineNorm() {
		return this.cosineNorm;
	}

	private double pearsonCorrelationTo(User that) {
		double sum = 0;
		double thisNorm = 0;
		double thatNorm = 0;
		for (Rating thisRating : this.ratings) {
			Rating thatRating = that.getRatingForItem(thisRating.getItem());
			if (thatRating != null) {
				double thisSummand = thisRating.getValue()
						- this.averageRating();
				double thatSummand = thatRating.getValue()
						- that.averageRating();
				sum += thisSummand * thatSummand;
				thisNorm += thisSummand * thisSummand;
				thatNorm += thatSummand * thatSummand;
			}
		}
		sum = sum == 0 ? 0 : sum / Math.sqrt(thisNorm * thatNorm);
		return Tools.keepBetween(sum, 0, 1);
		// return Tools.mapToInterval(sum, -1, 1, 0, 1);
	}

	private double tagSimilarityTo(User that) {
		double sum = 0;
		for (Average<Tag> entry : this.ratedTags) {
			for (Average<Tag> antry : that.ratedTags) {
				if (entry.getData().equals(antry.getData())) {
					sum += entry.getAverage() * antry.getAverage();
				}
			}
		}
		return sum <= 0 ? 0 : sum / (this.tagNorm() * that.tagNorm());
	}

	private double tagNorm() {
		return tagNorm;
	}

	private void updateTagNorm() {
		double sum = 0;
		for (Average<Tag> entry : this.ratedTags) {
			double tmp = entry.getAverage();
			sum += tmp * tmp;
		}
		this.tagNorm = Math.sqrt(sum);
	}

	private void updateAverageRating() {
		double sum = 0;
		for (Rating rating : this.ratings) {
			sum += rating.getValue();
		}
		this.averageRating = sum / this.ratings.size();
	}

	public double averageRating() {
		return averageRating;
	}
}

/**
 * This class represents a collection consisting of some sort of data (a key),
 * different values and their average. The values can be added and removed and
 * must range between {@code 0} and {@code 1}.
 */
class Average<T> implements Similar<Average<T>> {

	private T data;
	private double average;
	private List<Double> values = new ArrayList<Double>();

	Average(T data, double value) {
		if (value < 0 || value > 1) {
			throw new IllegalArgumentException(
					"The value must lie between 0 and 1. You passed a " + value
							+ ".");
		}
		this.data = data;
		values.add(value);
		calculateAverage();
	}

	void addValue(double value) {
		if (value < 0 || value > 1) {
			throw new IllegalArgumentException(
					"The value must lie between 0 and 1. You passed a " + value
							+ ".");
		}
		values.add(value);
		calculateAverage();
	}

	void removeValue(double value) {
		int index = values.indexOf(value);
		if (index != -1) {
			values.remove(index);
			calculateAverage();
		}
	}

	private void calculateAverage() {
		average = 0;
		for (double d : values) {
			average += d;
		}

		average /= values.size() > 0 ? values.size() : 1;
	}

	double getAverage() {
		return average;
	}

	T getData() {
		return data;
	}

	@Override
	public double similarityTo(Average<T> that) {
		return 1 - Math.abs(this.getAverage() - that.getAverage());
	}
}