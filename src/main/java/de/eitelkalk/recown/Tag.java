package de.eitelkalk.recown;

import de.eitelkalk.recown.tools.Tools;

/**
 * Represents some kind of content that can be contained in an {@link Item}. A
 * tag describes this {@link Item} as good as possible. Each tag is described by
 * a unique name. Tags can correspond to Twitter-like hashtags, categories, or
 * they can each even be a single word from a whole text if this text describes
 * the content. A good usage of tags depends on the use-case but the better the
 * tags describe the {@link Item}s, the better the recommendations will be.
 * 
 * @author eitelkalk
 *
 */
public class Tag {

	private final String name;
	private double weight;
	private int numberOfAppearances;

	/**
	 * Creates a {@link Tag} with a default {@code weight} of {@code 1}.
	 * 
	 * @param name
	 *            the name that uniquely describes the {@link Tag}.
	 * 
	 * @see #Tag(String, double)
	 */
	public Tag(String name) {
		this(name, 1);
	}

	/**
	 * Creates a {@link Tag} with a given unique {@code name} and an initial
	 * {@code weight} that describes its importance.
	 * 
	 * @param name
	 *            the name that uniquely describes the {@link Tag}.
	 * @param weight
	 *            the weight that describes the importance of this {@link Tag}.
	 * @see Database#updateWeights()
	 */
	public Tag(String name, double weight) {
		this.name = name;
		this.weight = weight;
		this.numberOfAppearances = 1;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getWeight() {
		return weight;
	}

	public String getName() {
		return name;
	}

	/**
	 * Counts how often this {@link Tag} appears in all the {@link Item}s.
	 * Whenever this {@link Tag} is added to an {@link Item} this should be
	 * called.
	 */
	public void increaseNumberOfAppearances() {
		numberOfAppearances++;
	}

	/**
	 * Returns how often this {@link Tag} appears in all the {@link Item}s.
	 * 
	 * @return the number of how often this {@link Tag} is used among all
	 *         {@link Item}s
	 */
	public int getNumberOfAppearances() {
		return numberOfAppearances;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Tag))
			return false;
		Tag other = (Tag) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name + "(" + numberOfAppearances + "|"
				+ String.format("%.2f", weight) + ")";
	}
}