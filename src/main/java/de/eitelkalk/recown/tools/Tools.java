package de.eitelkalk.recown.tools;

import java.util.Comparator;
import java.util.List;

import de.eitelkalk.recown.Similar;

/**
 * This class provides frequently called methods for general purposes.
 * 
 * @author eitelkalk
 *
 */
public class Tools {

	/**
	 * Linearly maps the value {@code x} from the interval
	 * {@code [minStart, maxStart]} to the corresponding value in the interval
	 * {@code [minTarget, maxTarget]}. Assumes that
	 * {@code minStart <= x <= maxStart} and {@code minTarget <= maxTarget}.
	 * 
	 * <p>
	 * If {@code minStart == maxStart}, then {@code minTarget} is returned.
	 * </p>
	 * 
	 * @param x
	 *            the value that should be mapped
	 * @param minStart
	 *            lower bound of the start interval
	 * @param maxStart
	 *            upper bound of the start interval
	 * @param minTarget
	 *            lower bound of the target interval
	 * @param maxTarget
	 *            upper bound of the target interval
	 * @return {@code y} in the interval {@code [minTarget, maxTarget]}
	 *         corresponding to the value {@code x} in the interval
	 *         {@code [minStart, maxStart]}.
	 */
	public static double mapToInterval(double x, double minStart,
			double maxStart, double minTarget, double maxTarget) {
		if (x < minStart || x > maxStart || minTarget > maxTarget
				|| minStart > maxStart) {
			throw new IllegalArgumentException(
					"Passed values must satify minStart <= x <= maxStart and minTarget <= maxTarget.");
		}
		if (maxStart - minStart == 0) {
			return minTarget;
		}
		return (x - minStart) / (maxStart - minStart) * (maxTarget - minTarget)
				+ minTarget;
	}

	public static double keepBetween(double value, double min, double max) {
		return Math.min(max, Math.max(value, min));
	}

	public static int keepBetween(int min, int max, int value) {
		value = value < min ? min : value;
		return value > max ? max : value;
	}

	/**
	 * Returns a list with the most similar elements in the given elements list.
	 * <p>
	 * The given list is modified and sometimes even returned itself.
	 * 
	 * @param root
	 *            Cannot be {@code null}.
	 * @param elements
	 * @param maxItemCount
	 */
	public static <T extends Similar<T>> List<T> getMostSimilarElements(T root,
			List<T> elements, int maxItemCount) {
		if (root == null)
			throw new NullPointerException("root cannot be null!");

		elements.sort(new Comparator<Similar>() {
			@Override
			public int compare(Similar s1, Similar s2) {
				return Double.compare(s1.similarityTo(root),
						s2.similarityTo(root));
			}
		});

		if (elements.size() > maxItemCount && maxItemCount > 0)
			return elements.subList(elements.size() - maxItemCount,
					elements.size());

		return elements;
	}
}
