package de.eitelkalk.recown;

/**
 * This class represents a rating a {@link User} gives an {@link Item}. Ratings
 * must range between {@code 0} and {@code 1}, where {@code 0} indicates the
 * {@link User} didn't like the {@link Item} at all and {@code 1} is the best
 * possible rating.
 * 
 * @author eitelkalk
 * @see User#rate(Item, double)
 *
 */
public class Rating implements Similar<Rating> {

	private final Item item;
	private double value;

	/**
	 * Creates a {@link Rating} a {@link User} gives an {@link Item}.
	 * 
	 * @param item
	 *            the {@link Item} a {@link User} rates.
	 * @param rating
	 *            A value between {@code 0} and {@code 1}, {@code 0} being the
	 *            worst and {@code 1} being the best rating.
	 * @throws IllegalArgumentException
	 *             If the passed {@code rating}-value is not between {@code 0}
	 *             and {@code 1}.
	 */
	public Rating(Item item, double rating) throws IllegalArgumentException {
		if (rating < 0 || rating > 1) {
			throw new IllegalArgumentException(
					"The rating-value must be between 0 and 1. You passed a "
							+ rating + ".");
		}
		this.item = item;
		this.value = rating;
	}

	public Item getItem() {
		return item;
	}

	/**
	 * Sets the value of this {@link Rating}. Must be a value between {@code 0}
	 * and {@code 1}.
	 * 
	 * @param rating
	 *            the rating-value between {@code 0} and {@code 1}.
	 * @throws IllegalArgumentException
	 *             If the passed {@code rating}-value is not between {@code 0}
	 *             and {@code 1}.
	 */
	public void setValue(double rating) {
		if (rating < 0 || rating > 1) {
			throw new IllegalArgumentException(
					"The rating-value must lie between 0 and 1. You passed a "
							+ rating + ".");
		}
		value = rating;
	}

	public double getValue() {
		return value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double similarityTo(Rating that) {
		return 1 - Math.abs(this.value - that.value);
	}
}