package de.eitelkalk.recown;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import de.eitelkalk.recown.tools.Tools;

/**
 * This class handles {@link Item}s and {@link User}s and provides methods for
 * recommending {@link Item}s based on various different aspects, mostly
 * {@link Similar}ity between {@link Item}s or {@link User}s.
 * 
 * @author eitelkalk
 */
public class Database {

	private final List<Item> items = new ArrayList<Item>();
	private final List<User> users = new ArrayList<User>();

	/**
	 * This hashmap is used to cache the idf-values in
	 * {@link #estimateWeightOf(Tag)}. This brings performance improvements when
	 * using bigger data sets.
	 */
	private HashMap<Tag, Double> cachedTagIdfs = new HashMap<>();

	/**
	 * The minimal value between users to be considered similar.
	 * 
	 * @see #setUserSimilarityThreshold(double)
	 */
	private double userSimilarityThreshold = 0.25;
	/**
	 * The minimal value for a rating to be considered as "good".
	 * 
	 * @see #setGoodRatingThreshold(double)
	 */
	private double goodRatungThreshold = 0.75;

	public void addItems(Collection<? extends Item> items) {
		this.items.addAll(items);
		updateWeights();
	}

	public void addItems(Item... items) {
		Collections.addAll(this.items, items);
		updateWeights();
	}

	public void addUsers(Collection<? extends User> users) {
		this.users.addAll(users);
	}

	public void addUsers(User... users) {
		Collections.addAll(this.users, users);
	}

	/**
	 * Updates the weights of all {@link Tag}s based on their importance. Should
	 * be called, whenever the {@link Tag}s of one or more {@link Item}s have
	 * been changed, i.&nbsp;e. a {@link Tag} has been added, removed or its
	 * number of appearances in one {@link Item} has changed.
	 * 
	 * <p>
	 * A {@link Tag} that is used in every {@link Item} is not considered as
	 * important as one used in only a few. Moreover, a {@link Tag} that appears
	 * often in an {@link Item} is more important to it than one that appears
	 * more rarely.
	 * </p>
	 * 
	 * <p>
	 * Technically speaking, this is calculated by the
	 * <a href="https://en.wikipedia.org/wiki/Tf%E2%80%93idf">term
	 * frequency-inverse document frequency</a>.
	 * </p>
	 */

	public void updateWeights() {
		cachedTagIdfs.clear();

		for (Item item : items) {
			for (Tag tag : item) {
				tag.setWeight(estimateWeightOf(tag));
			}
		}
	}

	private double estimateWeightOf(Tag tag) {
		Double idf = cachedTagIdfs.get(tag);
		if (idf == null) {
			idf = Math.log(((double) items.size()) / countItemsWithTag(tag));
			cachedTagIdfs.put(tag, idf);
		}

		return tag.getNumberOfAppearances() == 1 ? idf
				: ((1 + Math.log(tag.getNumberOfAppearances())) * idf);
	}

	private int countItemsWithTag(Tag tag) {
		int itemWithTagCount = 0;
		for (Item item : items) {
			if (item.contains(tag)) {
				itemWithTagCount++;
			}
		}
		return itemWithTagCount;
	}

	/**
	 * Based on the {@link Similar}ity between {@link Item}s, this method
	 * returns a {@link List} of {@link Item}s that are most similar to the
	 * given {@code item}. I.&nbsp;e. the {@link Item}s that have greatest
	 * {@link Item#similarityTo(Item)} to the given {@code item}.
	 * 
	 * <p>
	 * The {@link List} contains at most {@code numberOfItems} {@link Item}s.
	 * </p>
	 * 
	 * @param item
	 *            the {@link Item} with which the
	 *            {@link Item#similarityTo(Item)} is calculated.
	 * @param numberOfItems
	 *            maximum number of elements that should be contained in the
	 *            returned {@link List}.
	 * @return a {@link List} of {@link Item}s that are most similar to the
	 *         given {@code item}. {@code null} if no similar {@link Item} has
	 *         been found. E.&nbsp;g. this can happen if the given {@code item}
	 *         has no {@link Tag} in common with any other {@link Item} in this
	 *         {@link Database}. The {@link Item}s are sorted from most similar
	 *         to least similar.
	 */
	public List<Item> getMostSimilarItemsTo(Item item, int numberOfItems) {
		numberOfItems = Tools.keepBetween(0,
				items.size() == 0 ? 0 : (items.size() - 1), numberOfItems);
		List<Item> list = new ArrayList<Item>();
		for (Item i : items) {
			if (item != i) {
				list.add(i);
			}
		}
		return Tools.getMostSimilarElements(item, list, numberOfItems);
	}

	/**
	 * Finds the {@link User}s that are most similar to the given {@code user},
	 * that is where {@link User#similarityTo(User)} is greatest. Only
	 * {@link User}s having at least the similarity to the {@code user} set via
	 * {@link #setMinimalValueBetweenUsersToBeSimilar(double)} are contained in
	 * the returned {@link List}.
	 * 
	 * @param user
	 *            the {@link User} for which the most similar {@link User}s
	 *            should be found.
	 * @return a {@link List} of {@link User}s that are most similar to the
	 *         given {@code user}. {@code null} if no similar {@link User} has
	 *         been found. E.&nbsp;g. this can happen if all {@link User}s in
	 *         this {@link Database} have a smaller similarity to the given
	 *         {@code user} than the one set via
	 *         {@link #setMinimalValueBetweenUsersToBeSimilar(double)}. The
	 *         {@link User} are sorted from most similar to least similar.
	 */
	public List<User> getMostSimilarUsersTo(User user) {
		return getMostSimilarUsersTo(user, userSimilarityThreshold);
	}

	private List<User> getMostSimilarUsersTo(User user, double minimalMatch) {
		List<User> list = new ArrayList<User>();
		for (User u : users) {
			if (user != u && user.similarityTo(u) >= minimalMatch) {
				list.add(u);
			}
		}
		return Tools.getMostSimilarElements(user, list, list.size());
	}

	/**
	 * Based on {@link Similar}ity between {@link User}s, the {@link Item}s that
	 * are most likely to be rated good by the given {@code user} are found.
	 * 
	 * First, the most similar {@link User}s to the {@code user} are calculated
	 * and based on their {@link Rating}s the {@link Rating} for {@code user} is
	 * estimated. The {@link Item}s with the highest estimated {@link Rating}
	 * are returned.
	 * 
	 * The returned {@link List} contains at most
	 * {@code numberOfRecommendations} {@link Item}s.
	 * 
	 * @param user
	 *            the {@link User} for whom the recommendations should be given.
	 * @param numberOfRecommendations
	 *            maximal number of recommendations for the {@code user}.
	 * @return A {@link List} containing the {@link Item}s the {@code user} most
	 *         likely will rate best. {@code null} if no {@link Item} could be
	 *         found. There could be several reasons for this, e.&nbsp;g. the
	 *         {@code user} has already rated all {@link Item}s their most
	 *         similar {@link User}s rated. The {@link Item}s are sorted from
	 *         most best to worst.
	 */
	public List<Item> recommendBestItems(User user,
			int numberOfRecommendations) {
		numberOfRecommendations = Tools.keepBetween(0, items.size(),
				numberOfRecommendations);
		List<Rating> estimatedRatings = new ArrayList<Rating>(items.size());
		for (Item item : items) {
			if (!user.hasRated(item)) {
				Rating tmp = estimateRating(user, item);
				if (tmp != null)
					estimatedRatings.add(tmp);
			}
		}

		return extractItemsFromRatings(
				Tools.getMostSimilarElements(new Rating(null, 1),
						estimatedRatings, numberOfRecommendations));
	}

	/**
	 * Based on the {@link Item}s the {@code user} has rated best, the most
	 * similar {@link Item}s are found.
	 * 
	 * First, finds the best rated {@link Item}s for the {@code user} via
	 * {@link User#getBestRatedItems(int)} and then finds similar {@link Item}s
	 * to these via {@link #getMostSimilarItemsTo(Item, int)}.
	 * 
	 * At most {@code numberOfRecommendations} {@link Item} are returned.
	 * 
	 * @param user
	 *            the {@link User} for whom the {@link Item}s should be found.
	 * @param numberOfRecommendations
	 *            maximum number of {@link Item}s returned.
	 * @return A {@link List} of the {@link Item}s that are most similar to the
	 *         {@link Item}s {@code user} has rated best. {@code null} if no
	 *         such {@link Item} could be found. The {@link Item}s are ordered
	 *         randomly.
	 * @see #getMostSimilarItemsTo(Item, int)
	 * @see User#getBestRatedItems(int)
	 */
	public List<Item> recommendItemsSimilarToBestItems(User user,
			int numberOfRecommendations) {
		numberOfRecommendations = Tools.keepBetween(0, items.size(),
				numberOfRecommendations);
		int n = (int) Math.sqrt(numberOfRecommendations);
		List<Item> recommendations = new ArrayList<Item>();
		for (Item item : user.getBestRatedItems(n)) {
			for (Item i : getMostSimilarItemsTo(item, n)) {
				if (!recommendations.contains(i) && !user.hasRated(i)) {
					recommendations.add(i);
				}
			}
		}
		return shuffleAndReduceSize(numberOfRecommendations, recommendations);
	}

	/**
	 * Finds the {@link Item}s containing this {@code tag} which the
	 * {@code user} most likely will rate best.
	 * 
	 * @param user
	 *            the {@link User} for whom the recommendations should be given
	 * @param tag
	 *            the {@link Tag} that the returned {@link Item}s must contain
	 * @param numberOfRecommendations
	 *            maximum number of {@link Item}s returned
	 * @return A {@link List} of {@link Item}s where each {@link Item} contains
	 *         the {@code tag} and that are most likely to be rated good by the
	 *         {@code user}. {@code null} if no {@link Item} could be found.
	 * @see #estimateRating(User, Item)
	 */
	public List<Item> recommendItemsBasedOnTag(User user, Tag tag,
			int numberOfRecommendations) {
		numberOfRecommendations = Tools.keepBetween(0, items.size(),
				numberOfRecommendations);
		List<Rating> list = new ArrayList<Rating>(items.size());
		for (Item item : items) {
			if (item.contains(tag) && !user.hasRated(item)) {
				Rating tmp = estimateRating(user, item);
				if (tmp != null)
					list.add(tmp);
			}
		}

		return extractItemsFromRatings(Tools.getMostSimilarElements(
				new Rating(null, 1), list, numberOfRecommendations));
	}

	/**
	 * Returns a {@link List} of {@link Item}s that where liked by {@link User}s
	 * who also liked the given {@code item}. {@link Rating}s of the
	 * {@link User}s are only taken into account if the {@link User} rated the
	 * {@code item} with a value greater or equal to the one set via
	 * {@link #setMinimalValueForGoodRating(double)}.
	 * 
	 * @param item
	 *            the {@link Item} for which recommendations should be given.
	 * @param numberOfRecommendations
	 *            the maximum number of {@link Item}s contained in the returned
	 *            {@link List}.
	 * @return A {@link List} of {@link Item}s that have been rated good by the
	 *         {@link User}s who rated the given {@code item} good.
	 */
	public List<Item> recommendItemsBasedOnUserRatings(Item item,
			int numberOfRecommendations) {
		numberOfRecommendations = Tools.keepBetween(0, items.size(),
				numberOfRecommendations);
		List<Item> list = new ArrayList<Item>();
		for (User user : users) {
			Rating rating = user.getRatingForItem(item);
			if (rating != null && rating.getValue() >= goodRatungThreshold) {
				addGoodRatedItemsToList(item, numberOfRecommendations, list,
						user);
			}
		}
		return shuffleAndReduceSize(numberOfRecommendations, list);
	}

	private List<Item> shuffleAndReduceSize(int numberOfRecommendations,
			List<Item> list) {
		Collections.shuffle(list);
		if (list.size() > numberOfRecommendations) {
			return list.subList(0, numberOfRecommendations);
		}
		return list;
	}

	private void addGoodRatedItemsToList(Item item, int numberOfRecommendations,
			List<Item> list, User user) {
		for (Item i : user.getBestRatedItems(numberOfRecommendations)) {
			if (i != item && !list.contains(i) && user.getRatingForItem(i)
					.getValue() >= goodRatungThreshold) {
				list.add(i);
			}
		}
	}

	/**
	 * Estimates which {@link Rating} a {@code user} would give an {@code item}
	 * probably, based on the users that are most similar to them.
	 * 
	 * @param user
	 *            the {@link User} who rates the {@code item}
	 * @param item
	 *            the {@link Item} that will be rated by the {@code user}
	 * @return The {@link Rating} the {@code user} will probably give the
	 *         {@code item}. If the {@code user} has rated the {@code item}
	 *         already, it returns this {@link Rating}. If this is known
	 *         beforehand, better use {@link User#getRatingForItem(Item)}.
	 *         {@code null} if no {@link Rating} could be estimated. This can be
	 *         the case e.&nbsp;g. if {@link #getMostSimilarUsersTo(User)}
	 *         returns {@code null}.
	 * @see #getMostSimilarUsersTo(User)
	 */
	public Rating estimateRating(User user, Item item) {
		double sum = 0;
		double simSum = 0;

		// User has already rated the item
		Rating r = user.getRatingForItem(item);
		if (r != null) {
			return r;
		}

		for (User u : getMostSimilarUsersTo(user)) {
			Rating rating = u.getRatingForItem(item);
			if (rating != null) {
				double sim = user.similarityTo(u);
				sum += sim * (rating.getValue() - u.averageRating());
				simSum += sim;
			}
		}
		if (simSum > 0) {
			double estimatedValue = user.averageRating() + sum / simSum;
			return new Rating(item, Tools.keepBetween(estimatedValue, 0, 1));
		}
		return null;
	}

	private List<Item> extractItemsFromRatings(List<Rating> ratings) {
		List<Item> items = new ArrayList<Item>();
		ratings.forEach(rating -> items.add(rating.getItem()));
		return items;
	}

	/**
	 * Sets the weights that are used for the similarity calculation for all
	 * {@link User}s at once.
	 * 
	 * <p>
	 * The similarity {@link User#similarityTo(User)} is the weighted sum of
	 * different similarity calculations. The weights should be the same for all
	 * users. This method sets the weights for all users simultaneously. The
	 * weights must be greater or equal to {@code 0}. A negative weight
	 * indicates that the actual weight should not be changed. A {@code 0}
	 * weight obviously indicates that the corresponding similarity calculation
	 * should not contribute. The weights weight the following similarity
	 * calculations in this very order:
	 * <ul>
	 * <li>Cosine similarity between users &ndash; Represents how similar users
	 * are in their ratings. Very basic but effective similarity.</li>
	 * <li>Pearson similarity between users &ndash; Represents how similar users
	 * are in their ratings w.&nbsp;r.&nbsp;t. their respective average ratings.
	 * This is more exact than cosine similarity but requires more votings.</li>
	 * <li>Tag similarity between users &ndash; Represents how similar users are
	 * w.&nbsp;r.&nbsp;t. their rating of {@link Tag}s. Requires a good
	 * description of the {@link Item}s by {@link Tag}s, but then represents a
	 * good similarity on a lower level than the {@link Item}s themselves.</li>
	 * </ul>
	 * Initially, the weights for a {@link User} are {@code 1}, {@code 0},
	 * {@code 0}.
	 * 
	 * @param weights
	 *            the weights as described above
	 */
	public void setWeightsForUserSimilarityCalculation(double... weights) {
		this.forEachUser(
				user -> user.setWeightsForSimilarityCalculation(weights));
	}

	/**
	 * Sets the threshold for when two {@link User}s are considered to be
	 * similar enough. The method {@link #getMostSimilarUsersTo(User)} returns
	 * all {@link User}s who are similar enough to a given {@link User},
	 * i.&nbsp;e. where {@link User#similarityTo(User)} returns a value greater
	 * or equal to the {@code value} set here.
	 * 
	 * <p>
	 * By default, this is set to {@code 0.25}.
	 * </p>
	 * 
	 * @param value
	 *            The minimum value two {@link User}s are considered similar
	 *            enough. Must be a value between {@code 0} and {@code 1}. Set
	 *            to {@code 0} all {@link User}s are considered similar, set to
	 *            {@code 1} only {@link User}s who agreed on every
	 *            {@link Rating} are considered similar.
	 * @throws IllegalArgumentException
	 *             if the {@code value} is not between {@code 0} and {@code 1}.
	 */
	public void setUserSimilarityThreshold(double value) {
		if (value < 0 || value > 1) {
			throw new IllegalArgumentException(
					"Value must lie between 0 and 1.");
		}
		userSimilarityThreshold = value;
	}

	/**
	 * Sets the minimum value for when a {@link Rating} is considered as "good".
	 * This is e.&nbsp;g. used in the method
	 * {@link #recommendItemsBasedOnUserRatings(Item, int)} where a measurement
	 * of a "good" {@link Rating} is needed.
	 * 
	 * <p>
	 * By default this is set to {@code 0.75}.
	 * </p>
	 * 
	 * @param value
	 * @throws IllegalArgumentException
	 *             if the {@code value} is not between {@code 0} and {@code 1}.
	 */
	public void setGoodRatingThreshold(double value) {
		if (value < 0 || value > 1) {
			throw new IllegalArgumentException(
					"Value must lie between 0 and 1.");
		}
		goodRatungThreshold = value;
	}

	public void forEachItem(Consumer<? super Item> action) {
		items.forEach(action);
	}

	public List<Item> getItems() {
		return items;
	}

	public void forEachUser(Consumer<? super User> action) {
		users.forEach(action);
	}

	public List<User> getUsers() {
		return users;
	}
}