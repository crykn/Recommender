package de.eitelkalk.recown;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class represents the items that are handled by the {@link Database}.
 * Items can be rated by the {@link User}s and the content of these items is
 * described by {@link Tag}s.
 * 
 * @author eitelkalk
 *
 */
public class Item implements Iterable<Tag>, Similar<Item> {

	private final List<Tag> tags = new ArrayList<Tag>();

	/**
	 * Adds {@link Tag}s to this {@link Item} that describe its content. If the
	 * {@link Tag} is already contained,
	 * {@link Tag#increaseNumberOfAppearances()} is called.
	 * 
	 * @param tags
	 *            several {@link Tag}s describing this {@link Item}
	 */
	public void addTags(Tag... tags) {
		for (Tag t : tags) {
			int index = this.tags.indexOf(t);
			if (index != -1) {
				this.tags.get(index).increaseNumberOfAppearances();
			} else {
				this.tags.add(t);
			}
		}
	}

	public boolean contains(Tag tag) {
		return tags.contains(tag);
	}

	public int getNumberOfTags() {
		return tags.size();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * The similarity is calculated by the cosine-similarity between
	 * {@link Item}s based on their {@link Tag}s and their respective weights.
	 */
	@Override
	public double similarityTo(Item that) {
		double sum = 0;
		for (Tag tig : this) {
			for (Tag tag : that) {
				if (tig.equals(tag)) {
					sum += tig.getWeight() * tag.getWeight();
				}
			}
		}
		return sum <= 0 ? 0
				: (sum / (this.calculateNorm() * that.calculateNorm()));
	}

	private double calculateNorm() {
		double sum = 0;
		for (Tag tag : this) {
			sum += tag.getWeight() * tag.getWeight();
		}
		return Math.sqrt(sum);
	}

	@Override
	public Iterator<Tag> iterator() {
		return tags.iterator();
	}

	@Override
	public String toString() {
		return tags.stream().map(Object::toString)
				.collect(Collectors.joining(", "));
	}
}