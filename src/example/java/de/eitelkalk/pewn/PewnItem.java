package de.eitelkalk.pewn;

import de.eitelkalk.recown.Item;

public class PewnItem extends Item {

	private final String name;
	private final long id;

	public PewnItem(String name, long id) {
		this.name = name;
		this.id = id;
	}

	public long getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PewnItem other = (PewnItem) obj;
		if (id != other.id)
			return false;
		return true;
	}
}