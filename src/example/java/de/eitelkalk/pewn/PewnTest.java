package de.eitelkalk.pewn;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonSyntaxException;

import de.damios.jpapi.model.Hashtag;
import de.damios.jpapi.model.Project;
import de.damios.jpapi.model.Project.OrderedBy;
import de.damios.jpapi.model.Rating;
import de.eitelkalk.recown.Database;
import de.eitelkalk.recown.Item;
import de.eitelkalk.recown.Tag;
import de.eitelkalk.recown.User;

public class PewnTest {

	public static void main(String[] args) {
		Database pewn = createDatabase();
		pewn.setUserSimilarityThreshold(0);
		pewn.setWeightsForUserSimilarityCalculation(1, 0, 0);
		// Play with the weights. try (1, 2, 1), (2, 1, 0)...
		// Also, consider getting recommendations for (1, 0, 0), (0, 1, 0) and
		// (0, 0, 1) distinctly and then combine all the recommendations into
		// one single list

		String today = LocalDateTime.now()
				.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
		System.out.println("# Recommendations for Pewn &ndash; " + today);
		printTableOfContents();

		System.out.println(
				"<a name=\"content\"></a>\n## Recommendations for games");
		System.out.println("<a name=\"sim\"></a>\n### Similar games");
		pewn.forEachItem(item -> printBestRecommendationsFor(item, pewn));

		System.out.println(
				"<a name=\"itemItem\"></a>\n### Users who liked X also liked Y");
		pewn.forEachItem(item -> printUsersWhoLikedXAlsoLikedY(item, pewn));

		System.out.println(
				"<a name=\"collaborative\"></a>\n## Recommendations for users");
		System.out.println("<a name=\"best\"></a>\n### Best recommendations");
		pewn.forEachUser(user -> printBestRecommendationsFor(user, pewn));

		System.out.println(
				"<a name=\"similar\"></a>\n### Similar games to the best rated");
		pewn.forEachUser(user -> printSimilarItemsToBestRatedFor(user, pewn));

		System.out.println(
				"<a name=\"tags\"></a>\n### Recommendations based on the (implicitly) best rated tags");
		pewn.forEachUser(user -> printRecommendationsBasedOnTags(user, pewn));
	}

	private static void printTableOfContents() {
		System.out.println("## Table of contents");
		System.out.println("1. [Recommendations for games](#content)");
		System.out.println("    1. [Similar games](#sim)");
		System.out
				.println("    2. [Users who liked X also liked Y](#itemItem)");
		System.out.println("2. [Recommendations for users](#collaborative)");
		System.out.println("    1. [Best recommendations](#best)");
		System.out
				.println("    2. [Similar games to the best rated](#similar)");
		System.out.println(
				"    3. [Recommendations based on the (implicitly) best rated tags](#tags)");
		System.out.println();
	}

	private static void printUsersWhoLikedXAlsoLikedY(Item item,
			Database pewn) {
		System.out.println("**" + ((PewnItem) item).getName() + ":**");
		List<Item> items = pewn.recommendItemsBasedOnUserRatings(item, 5);
		for (int index = 0; index < items.size(); index++) {
			PewnItem i = (PewnItem) items.get(index);
			System.out.println((index + 1) + ". " + i.getName());
		}
		System.out.println();
	}

	private static void printRecommendationsBasedOnTags(User user,
			Database pewn) {
		System.out.println("**" + ((PewnUser) user).getName() + ":**");
		for (Tag tag : user.getBestRatedTags(5)) {
			System.out.print("- Based on *" + tag.getName() + ":* ");
			for (Item i : pewn.recommendItemsBasedOnTag(user, tag, 3)) {
				System.out.print(((PewnItem) i).getName() + ", ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}

	private static void printSimilarItemsToBestRatedFor(User user,
			Database pewn) {
		System.out.println("**" + ((PewnUser) user).getName() + ":**");

		List<Item> items = pewn.recommendItemsSimilarToBestItems(user, 10);
		for (int i = 0; i < items.size(); i++) {
			PewnItem item = (PewnItem) items.get(i);
			System.out.println((i + 1) + ". " + item.getName());
		}
		System.out.println();
	}

	private static void printBestRecommendationsFor(User user, Database pewn) {
		System.out.println("**" + ((PewnUser) user).getName() + ":**\n");

		List<User> nearestUsers = pewn.getMostSimilarUsersTo(user);
		if (nearestUsers.size() > 0) {
			System.out.println("*Similar users:*");
			for (int i = 0; i < nearestUsers.size(); i++) {
				PewnUser u = (PewnUser) nearestUsers.get(i);
				System.out.print((i + 1) + ". " + u.getName());
				System.out.printf(" (%.2f)%n", u.similarityTo(user));
			}
			System.out.println("\n*Recommended Games:*");
		}
		List<Item> items = pewn.recommendBestItems(user, 5);
		for (int i = 0; i < items.size(); i++) {
			PewnItem item = (PewnItem) items.get(i);
			System.out.print((i + 1) + ". " + item.getName());
			de.eitelkalk.recown.Rating rating = pewn.estimateRating(user, item);
			if (rating != null) {
				System.out.printf(" (%.2f)", rating.getValue());
			}
			System.out.println();
		}
		System.out.println();
	}

	private static void printBestRecommendationsFor(Item item, Database pewn) {
		if (item.getNumberOfTags() > 0) {
			System.out.println("**" + ((PewnItem) item).getName() + ":**");
			List<Item> items = pewn.getMostSimilarItemsTo(item, 5);
			for (int index = 0; index < items.size(); index++) {
				PewnItem i = (PewnItem) items.get(index);
				System.out.print((index + 1) + ". " + i.getName());
				System.out.printf(" (%.2f)%n", i.similarityTo(item));
			}
			System.out.println();
		}
	}

	private static Database createDatabase() {
		Database pewn = new Database();
		List<PewnItem> items = new ArrayList<PewnItem>();
		List<PewnUser> users = new ArrayList<PewnUser>();
		try {
			Project[] projects = Project.getAll(OrderedBy.CREATION_DATE);
			for (Project project : projects) {
				createItemAndUser(items, users, project);
			}
		} catch (JsonSyntaxException | IOException e) {
			e.printStackTrace();
		}
		pewn.addItems(items);
		pewn.addUsers(users);
		return pewn;
	}

	private static void createItemAndUser(List<PewnItem> items,
			List<PewnUser> users, Project project) throws IOException {
		PewnItem item = createPewnItem(project);
		items.add(item);
		for (Rating rating : project.getRatings()) {
			rateItemAndCreateUserIfNecessary(users, item, rating);
		}
	}

	private static void rateItemAndCreateUserIfNecessary(List<PewnUser> users,
			PewnItem item, Rating rating) {
		PewnUser user = new PewnUser(rating.getAuthor().getName(),
				rating.getAuthor().getId());
		int index = users.indexOf(user);
		if (index != -1) { // user existed before
			user = users.get(index);
		}
		user.rate(item, rating.getRating() / 10f);
		if (index == -1) { // user did not exist before
			users.add(user);
		}
	}

	private static PewnItem createPewnItem(Project project)
			throws JsonSyntaxException, IOException {
		PewnItem item = new PewnItem(project.getName(), project.getId());
		for (Hashtag tag : project.getHashtags()) {
			String name = tag.getTag();
			item.addTags(new Tag(name));
		}
		return item;
	}
}