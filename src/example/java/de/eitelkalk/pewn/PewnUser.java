package de.eitelkalk.pewn;

import de.eitelkalk.recown.User;

public class PewnUser extends User {

	private final String name;
	private final long id;

	public PewnUser(String name, long id) {
		this.name = name;
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PewnUser other = (PewnUser) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getName() {
		return name;
	}
}